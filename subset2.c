void sub_dup(int *s_nums, int *s_nums_col, int s_numsSize, int returnSize, int *returnColumnSizes, int **out);
int sort(int *nums, int numsSize, int *s_nums, int *s_nums_col);

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** subsetsWithDup(int* nums, int numsSize, int* returnSize, int** returnColumnSizes){
    int s_numsSize;
    int *s_nums, *s_nums_col;
    int i;
    int **out;

    s_nums = malloc(sizeof(int) * numsSize);
    s_nums_col = malloc(sizeof(int) * numsSize);
    s_numsSize = sort(nums, numsSize, s_nums, s_nums_col);

    *returnSize = 1;
    for (i = 0; i < s_numsSize; i++) {
        *returnSize *= (1 + s_nums_col[i]);
    }

    out = malloc(sizeof(int *) * (*returnSize));
    *returnColumnSizes = malloc(sizeof(int) * (*returnSize));

    sub_dup(s_nums, s_nums_col, s_numsSize, *returnSize, *returnColumnSizes, out);

    return out;
}

void sub_dup(int *s_nums, int *s_nums_col, int s_numsSize, int returnSize, int *returnColumnSizes, int **out)
{
    int cur_dup, cur_index, cur_size;
    int i, j, k;

    cur_dup = 1 + s_nums_col[s_numsSize - 1];
    sub_dup(s_nums, s_nums_col, s_numsSize - 1, returnSize / cur_dup, returnColumnSizes, out);

    for (i = 1; i < cur_dup; i++) {
        for (j = 0; j < returnSize / cur_dup; j++) {
            cur_index = (returnSize / cur_dup) * i + j;
            cur_size = returnColumnSizes[j] + i;

            returnColumnSizes[cur_index] = cur_size;
            out[cur_index] = malloc(sizeof(int) * cur_size);
            if (cur_size > i) {
                memcpy(out[cur_index], out[j], sizeof(int) * (cur_size - i));
            }
            for (k = i; k > 0; k--) {
                out[cur_index][cur_size - k] = s_nums[s_numsSize - 1];
            }
        }
    }

    return;
}

int sort(int *nums, int numsSize, int *s_nums, int *s_nums_col)
{
    int i, j;
    int tmp;

    if (numsSize == 0) {
        return 0;
    }

    for (i = 1; i < numsSize; i++) {
        for (j = i; j > 0; j--) {
            if (nums[j] < nums[j - 1]) {
                tmp = nums[j];
                nums[j] = nums[j - 1];
                nums[j - 1] = tmp;
            } else {
                continue;
            }
        }
    }

    j = 0;
    for (i = 0; i < numsSize; i++) {
        s_nums_col[i] = 0;
        s_nums[i] = 0;
    }
    for (i = 0; i < numsSize; i++) {
        if (i != 0 && nums[i] != nums[i - 1]) {
            s_nums[++j] = nums[i];
        } else if (i == 0) {
            s_nums[j] = nums[i];
        }
        s_nums_col[j]++;
    }

    return (j + 1);
}