#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

int coinChange(int* coins, int coinsSize, int amount);
int best(int* coins, int coinsSize, int amount, int *fp, int *rec);
void timeout_print(void);

void timeout_print(void)
{
    printf("3 minutes timed out,,,exit now!\n");
    return;
}

int main()
{
    int i;
    int amount, coinsSize;
    int *coins;
    int ret = -1;
    clock_t begin, end;

    scanf("%d", &coinsSize);
    coins = malloc(sizeof(int) * coinsSize);

    for (i = 0; i < coinsSize; i++) {
        scanf("%d ", &coins[i]);
    }
    scanf("%d", &amount);

    //atexit(timeout_print);
    signal(SIGALRM, _exit);
    alarm(3 * 60);

    begin = clock();
    ret = coinChange(coins, coinsSize, amount);
    end = clock();

    printf("%d\n", ret);
    printf("time:%d\n", (end - begin) / CLOCKS_PER_SEC);

    return 0;
}

int coinChange(int* coins, int coinsSize, int amount)
{
    int ret = -1;
    int *fp, *rec;

    fp = malloc(sizeof(int) * (amount + 1));
    memset(fp, 0, sizeof(int) * (amount + 1));
    rec = malloc(sizeof(int) * (amount + 1));
    ret = best(coins, coinsSize, amount, fp, rec);

    return ret;
}

int best(int* coins, int coinsSize, int amount, int *fp, int *rec)
{
    int i;
    int ret, min;

    if (fp[amount])
        return rec[amount];

    if (amount == 0)
        return 0;

    min = -1;
    for (i = 0; i < coinsSize; i++) {
        if (amount >= coins[i]) {
            ret = best(coins, coinsSize, amount - coins[i], fp, rec);
            if (ret != -1 && (min == -1 || min > ret + 1))
                min = ret + 1;
        }
    }

    fp[amount] = 1;
    rec[amount] = min;

    return min;
}
