#include <stdio.h>
#include <string.h>
#include <math.h>

void sub(int *nums, int numsSize, int returnSize, int** returnColumnSizes, int **out);
int** subsets(int* nums, int numsSize, int* returnSize, int** returnColumnSizes);

int main()
{
    int n;
    int *nums;
    int i, j;
    int **ret, **ret_col;
    int size;

    scanf("%d", &n);
    nums = malloc(sizeof(int) * n);
    for (i = 0; i < n; i++) {
        scanf("%d", &nums[i]);
    }

    size = pow(2, n);
    ret_col = malloc(sizeof(int *) * size);
    ret = subsets(nums, n, &size, ret_col);

    printf("=====================\n");
    for (i = 0; i < size; i++) {
        printf("[");
        for(j = 0; j < *ret_col[i]; j++) {
            printf("%d ", ret[i][j]);
        }
        printf("]\n");
    }

    free(nums);
    for(i = 0; i < size; i++) {
        free(ret[i]);
        free(ret_col[i]);
    }
    free(ret);
    free(ret_col);

    return 0;
}

int** subsets(int* nums, int numsSize, int* returnSize, int** returnColumnSizes)
{
    int **out;

    *returnSize = pow(2, numsSize);
    
    out = malloc(sizeof(int *) * (*returnSize));

    sub(nums, numsSize, *returnSize, returnColumnSizes, out);

    return out;
}

void sub(int *nums, int numsSize, int returnSize, int** returnColumnSizes, int **out)
{
    int i;
    int **cur, **cur_col;
    int cur_size;

    if (numsSize == 0) {
        *returnColumnSizes = malloc(sizeof(int));
        **returnColumnSizes = 0;
        *out = malloc(sizeof(int));
        **out = NULL;
        return;
    }

    sub(nums, numsSize - 1, returnSize / 2, returnColumnSizes, out);
    cur = out + returnSize / 2;
    cur_col = returnColumnSizes + returnSize / 2;
    for (i = 0; i < returnSize / 2; i++) {
        cur_col[i] = malloc(sizeof(int));
        *(cur_col[i]) = *(returnColumnSizes[i]) + 1;

        cur_size = *(cur_col[i]);

        cur[i] = malloc(sizeof(int) * cur_size);
        if (cur_size > 1) {
            memcpy(cur[i], out[i], sizeof(int) * (cur_size - 1));
        }
        cur[i][cur_size - 1] = nums[numsSize - 1];
    }
    return;
}
