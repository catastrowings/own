#!/bin/bash

if [ "$1" == "" ]
then
    path=$(pwd)
else
    path=$1
fi

find ${path} -name "*.c" -o -name "*.l" -o -name "*.y" -o -name "*.cc" -o -name "*.h" -o -name "*.cpp" -o -name "*.sh" -o -name "*.py" -o -name "*.pl" -o -name "*.S" -o -name "*Makefile*" -o -name "*.include" > cscope.files

cscope -bkq -i cscope.files &>/dev/null
